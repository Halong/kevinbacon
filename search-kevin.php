<?php include("top.html"); ?>
<?php
    include './common.php';
    //To avoid someone trying to send data by other methods
    if($_SERVER['REQUEST_METHOD']=='GET'){
        $error = [];
        // if first name is empty, add a String error to $error array
        if(empty($_GET['firstname']))
        {
            $error[] = "You must enter the first name!";
        }
        // if last name is empty, add a String error to $error array
        if(empty($_GET['lastname'])){
            $error[] = "You must enter the last name!";
        }
        //if the array error contains element(s), show all elements and exit.
        if(!empty($error)){?>
        <p>
            <?php
                echo 'INVALID INPUT DATA!<br>';
                foreach ($error as $e){
                    echo $e.'<br>';
                }
                echo "Comeback to <a href='javascript:history.go(-1)'>previous page</a> to check again!";
                include("bottom.html"); 
                exit();
            ?>
        </p>
        <?php
        }
        else{
            try{
                //trim the input data
                $fname = trim($_GET['firstname']);
                $lname = trim($_GET['lastname']);
                $fullname = $fname.' '.$lname;
                $count = 0;
              
                //select name and year from table movies named mov. We join mov with rol1(roles of actor 1), act1(data of actor 1 from actors table)
                //rol2 (roles of actor 2), act2 (data of actor 2 from actors table)
                //the condition are actor 1's firstname and lastname like Kevin Bacon, actor 2's firstname and last name like input data
                //both movies'id from 2 roles table of 2 actors should be identical
                //Order by movie's year descendingly and breaking tie by movies' name ascendingly
                /*
                $query = 'SELECT movies.name, movies.year'
                        . ' FROM movies'
                        . ' inner join roles as rol1 on movies.id = rol1.movie_id'
                        . ' inner join actors as act1 on rol1.actor_id = act1.id'
                        . ' inner join roles as rol2 on movies.id = rol2.movie_id'
                        . ' inner join actors as act2 on rol2.actor_id = act2.id'
                        . ' Where act1.first_name LIKE "Kevin" and act1.last_name LIKE "Bacon" and act2.first_name LIKE :fname and act2.last_name LIKE :lname and rol1.movie_id = rol2.movie_id ORDER BY movies.year desc, movies.name asc';
                */
                /*
                 * In this way, I have used the correlated subqueries to boost up the speed of matching.
                 * The process is:
                 *      Firstly, run the query from the outermost, in this case it selects movies.id, movies.name, movies.year
                 *      Then, run inner queries from outside to inside. All results from subqueries after executing will be put in WHERE clause to assess 
                 */     
                $query = "SELECT movies.id, movies.name, movies.year FROM movies"
                        . " WHERE movies.id IN (SELECT tbl.mov_id FROM (SELECT movie_id as mov_id, count(movie_id) as count_mov"
                        . " FROM roles WHERE actor_id IN (SELECT id FROM actors WHERE CONCAT_WS(' ',first_name,last_name)"
                        . " IN ('Kevin Bacon',:fullname)) GROUP BY mov_id) as tbl WHERE tbl.count_mov=2 ORDER BY movies.year DESC)"
                        . " ORDER BY movies.year DESC, movies.name ASC";
                // To avoid SQL injection
                $rows = $db->prepare($query);
                
                //$rows->bindParam(":fname",$fname);
                //$rows->bindParam(":lname",$lname);
                
                // Use bind param to insert input data into placeholders to avoid SQL injection
                $rows->bindParam(":fullname",$fullname,PDO::PARAM_STR);
                $rows->execute();
                //we count the rows when prepare the query, if it equals to or higher than 1, assign count with the number of rows
                if($rows->rowCount()>=1){
                    $count = $rows->rowCount();
                }
                // else if it equals to zero, assign count with zero
                else if($rows->rowCount()==0){
                    $count = 0;
                }// if $fullname is kevin bacon (case-insensitive), assign count with -1
                if(strcasecmp($fullname, "kevin bacon")==0){
                    $count = -1;
                }
            }catch (PDOException $e) {
                echo "Failed to read the database: ".$e->getMessage();
                exit('<br>Comeback to <a href="javascript:history.go(-1)">previous page</a> to check again!');
            }
        }
    }else{
        exit("Something went wrong while searching. Please <a href='javascript:history.go(-1)'>check again</a>");
    }
?>
<?php   //if $count >= 1 then show the table
        if ($count>=1) {
        //we initialize a variable to be the index of rows, later when using foreach, the index will be increased by 1.    
        $index = 0;?>
	<h1>Result(s) for <?php echo ucfirst($fname)." ".ucfirst($lname); ?></h1>
		<table>
                    <caption>Film(s) with <?=ucfirst($fname)?> <?=ucfirst($lname)?> and Kevin Bacon</caption>
		<tr>
			<th>#</th>
			<th>Title</th>
			<th>Year</th>
		</tr>
		<?php foreach ($rows as $r){ ?>
		<tr>
			<td><?= ++$index; ?></td>
			<td><?= $r['name']; ?></td>
			<td><?= $r['year']; ?></td>
		</tr>
		<?php } ?>
		</table>	
		<?php } 
        // if $count==0 then show message "..." wasn't in any films with Kevin Bacon
        else if ($count==0){?>
                <p><?=ucfirst($fname)?> <?=ucfirst($lname)?> wasn't in any films with Kevin Bacon.</p>
        <?php }
        // if $count <0, show message Please use search all movies feature to search all Kevin Bacon's movies
        else { ?>
            <p>Please use search all movies feature to search all Kevin Bacon's movies.</p>
        <?php }
        ?>
<?php include("bottom.html"); ?>
