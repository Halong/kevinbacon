<?php include("top.html"); ?>
<?php
    include './common.php';
    //To avoid someone trying to send data by other methods
    if($_SERVER['REQUEST_METHOD']=='GET'){
        $error = [];
        // if first name is empty, add a String error to $error array
        if(empty($_GET['firstname']))
        {
            $error[] = "You must enter the first name!";
        }
        // if last name is empty, add a String error to $error array
        if(empty($_GET['lastname'])){
            $error[] = "You must enter the last name!";
        }
        //if the array error contains element(s), show all elements and exit.
        if(!empty($error)){?>
        <p>
            <?php
                echo 'INVALID INPUT DATA!<br>';
                foreach ($error as $e){
                    echo $e.'<br>';
                }
                echo "Comeback to <a href='javascript:history.go(-1)'>previous page</a> to check again!";
                include("bottom.html"); 
                exit();
            ?>
        </p>
        <?php
        }
        else{
            try{
                //trim the input data
                $fname = trim($_GET['firstname']);
                $lname = trim($_GET['lastname']);
                $count = 0;
                //Select name and year from movies, join movies, roles, actors, the condition is first name and last name of actor have to be identical to input data
                //Order by movie's year descendingly and breaking tie by movies' name ascendingly
                $query = 'select movies.name, movies.year from movies join roles on movies.id = roles.movie_id'
                        .' join actors on roles.actor_id = actors.id '
                        .'where actors.first_name LIKE :fname and actors.last_name LIKE :lname ORDER BY movies.year desc, movies.name asc';
                //Prepare the above query
                $rows = $db->prepare($query);
                //Use bind param to insert input data into placeholders to avoid SQL injection
                $rows->bindParam(":fname",$fname,PDO::PARAM_STR);
                $rows->bindParam(":lname",$lname,PDO::PARAM_STR);
                $rows->execute();
                //we count the rows when prepare the query, if it's equal or higher than 1, assign count with the number of rows
                if($rows->rowCount()>=1){
                    $count = $rows->rowCount();
                }
                else{
                    $count = 0;
                }
            }catch (PDOException $e) {
                echo "Failed to read the database: ".$e->getMessage();
                exit('<br>Comeback to <a href="javascript:history.go(-1)">previous page</a> to check again!');
            }
        }
    }else{
        exit("Something went wrong while searching. Please <a href='javascript:history.go(-1)'>check again</a>");
    }
?>
<?php   //if $count >= 1 then show the table
        if ($count>=1) {
        //we initialize a variable to be the index of rows, later when using foreach, the index will be increased by 1.
        $index = 0;?>

	<h1>Result(s) for <?php echo ucfirst($fname)." ".ucfirst($lname); ?></h1>
		<table>
                    <caption>Movie(s) of <?=ucfirst($fname)?> <?=ucfirst($lname)?></caption>
		<tr>
			<th>#</th>
			<th>Title</th>
			<th>Year</th>
		</tr>
		<?php foreach ($rows as $r){ ?>
		<tr>
			<td><?= ++$index; ?></td>
			<td><?= $r['name']; ?></td>
			<td><?= $r['year']; ?></td>
		</tr>
		<?php } ?>
		</table>	
		<?php } 
        else{ //if $count<=0 then show not found
            ?>
                <p><?=ucfirst($fname)?> <?=ucfirst($lname)?> not found.</p>
        <?php }?>
<?php include("bottom.html"); ?>
