3.<?php
// Establish the connection by PDO
try {
       $db = new PDO ("mysql:host=localhost;dbname=imdb", "root", "");
       $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
       //Set error mode for PDO
       $db->setAttribute (PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
       //Execute the query set names utf8 to avoid utf8 font error when querying
       $db->exec("set names utf8");
    } 
catch (PDOException $e) {
        //When cannot establish the connection, a PDOException will be caught, it shows the message below and exit.
        echo "Failed to establish the connection: ".$e->getMessage();
        exit();
    }
    
    